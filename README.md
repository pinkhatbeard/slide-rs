# slide-rs
## Static Slideshow Generator
- Write slides in markdown
- create templates in jinja
- `slide-rs --filename index.md --output slides.html`
- magic

Ultimately this should work a lot like `jekyll` or `gutenberg` meets `reveal.js`
or `remark` with the possibility of being comparable to a cross-platform deckset.

If all slides are fairly straight forward and don't need a lot of changes to templates,
one file is all that's needs. `slides.md` -> transforms into `slides.html`.

If more control and customization is wanted each slide could be a separate file.

Every slide is written in a similar manner to a static site generator:

```
// the first lines of the file are metadata
default-template: "base.html"
title: "Sally Smith goes to Washington"
author: "Sally Smith"
contact: "sally@example.com"
url: "github.com/example/sallysslides"

syntax-hightlighting = true
defautl-syntax-language = "python"

+++  // delinates meta vs content

  # Slide 1 Header
  - this is markdown
  - this is a list

---  // slide separator
  # Slide 2 Header
  `slides-rs` come with templates and themes built in, but makes overriding quite simple.

---  // to override the default-template
  template: "two-column.html"
  +++

  # Slide 3 Header

  {% block column1 %}
    This slide is now a different template style.
  {% endblock %}

  {% block column1 %}
    Templates are written in a jinja style syntax.
  {% endblock %}

---

  # Other Features
  - shortcode/macros
  - generates static site (no need to even take a laptop to the conference!)
    - share the URL and let your audience follow along
      - immensly helpful with reading code or reading text in general
      - slides can sync with audience copies
      - audience can go back to catch what they missed
  - can cache all assets offline
```

## Features
- [ ] markdown to slideshow
- [ ] built in templates/themes
- [ ] template/themes are easy to write with a jinja style syntax
- [ ] single file slideshow
  - [ ] easy to override theme on a slide by slide basis
- [ ] cross platform
- [ ] development server with live reload
- [ ] shortcode/macros
  - [ ] easily embed videos
- [ ] generates static site (no need to even take a laptop to the conference!)
  - [ ] share the URL and let your audience follow along
    - [ ] immensely helpful with reading code or reading text in general
    - [ ] slides can sync with audience copies
    - [ ] audience can go back to catch what they missed
- [ ] cache all assets offline
- [ ] presentation mode
  - [ ] timer (with manual reset)
  - [ ] configurable
- [ ] templates can be set to different aspect rations without recompiling
- [ ] compile time warnings
  - [ ] "WARNING! Too much text on bottom third of slide is hard to read."
