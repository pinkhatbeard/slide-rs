theme = "base"
slideshow_template = "base.html"
default_slide_template = "slide.html"
title = "Sally Smith goes to Washington"
author = "Sally Smith"
contact = "sally@example.com"
url = "github.com/example/sallysslides"
default_syntax_language = "python"
syntax_highlighting = true

+++

  # Slide 1

  this is content

---

  # Slide 2

  ## List Header
  - item
  - item 2

  `echo tacos`

  ```python
  def tacos():
      print("tacos")
  ```

---
  # Slide 3
  this is content
