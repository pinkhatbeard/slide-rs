extern crate clap;

use clap::{App, Arg};

use std::error::Error;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

extern crate slide_rs;
use slide_rs::Slideshow;

fn main() {
    let matches = App::new("slide-rs")
        .version("0.1.0")
        .author("Cameron Dershem <cameron@pinkhatbeard.com>")
        .about("Markdown -> Slideshow with magic.")
        .arg(
            Arg::with_name("filename")
                .short("f")
                .long("filename")
                .takes_value(true)
                .help("Path to markdown file."),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .takes_value(true)
                .help("Path to output file."),
        )
        .get_matches();

    let filename = matches.value_of("filename").unwrap_or("slides.md");
    let path = Path::new(filename);
    let display = path.display();

    let mut file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why.description()),
        Ok(file) => file,
    };

    let mut markdown = String::new();
    let html_output = match file.read_to_string(&mut markdown) {
        Err(why) => panic!("couldn't read {}: {}", display, why.description()),
        Ok(_) => Slideshow::new(&markdown).render_slideshow(),
    };

    let output_filename = matches.value_of("output").unwrap_or("output.html");
    let output_path = Path::new(output_filename);
    let output_display = output_path.display();

    let mut output_file = match File::create(&output_path) {
        Err(why) => panic!("couldn't create {}: {}", output_display, why.description()),
        Ok(file) => file,
    };

    match output_file.write_all(html_output.as_bytes()) {
        Err(why) => panic!(
            "couldn't write to {}: {}",
            output_display,
            why.description()
        ),
        Ok(_) => println!("successfully wrote to {}", output_display),
    };
}
