#[macro_use]
extern crate serde_derive;
extern crate tera;
extern crate toml;

extern crate pulldown_cmark;

use pulldown_cmark::{html, Parser};
use tera::{Context, Tera};

#[derive(Deserialize, Debug)]
pub struct Config {
    pub theme: Option<String>,
    pub slideshow_template: Option<String>,
    pub default_slide_template: Option<String>,
    pub title: Option<String>,
    pub author: Option<String>,
    pub contact: Option<String>,
    pub url: Option<String>,
    pub syntax_highlighting: Option<bool>,
    pub default_syntax_language: Option<String>,
}

impl Config {
    pub fn new(meta: &str) -> Config {
        let config: Config = toml::from_str(meta).unwrap();

        config
    }
}

pub struct Slideshow {
    pub config: Config,
    pub slides: Vec<Slide>,
}

impl Slideshow {
    pub fn new(text: &str) -> Slideshow {
        let (config, slides) = Slideshow::split_meta(text);

        Slideshow {
            config: Config::new(config),
            slides: Slideshow::split_slides(slides),
        }
    }

    pub fn render_slides(slides: &Vec<Slide>) -> String {
        let mut html = String::new();

        for slide in slides {
            html.push_str(&Slide::render_template(
                &Slide::markdown_to_html(&slide.raw),
                &slide.number,
                &slide.template,
            ));
        }

        html
    }

    pub fn render_slideshow(&self) -> String {
        let tera = Tera::new("./templates/**/*").unwrap();
        let mut ctx = Context::new();

        ctx.add("slides", &Slideshow::render_slides(&self.slides));
        ctx.add("title", &self.config.title);

        let temp = tera.render(
            &self.config.slideshow_template.as_ref().unwrap().clone(),
            &ctx,
        ).unwrap();

        temp
    }

    pub fn split_meta(text: &str) -> (&str, &str) {
        let separator = "+++";
        let split_text: Vec<&str> = text.split(separator).collect();
        let (meta, content) = (split_text[0], split_text[1]);

        (meta, content)
    }

    pub fn split_slides(text: &str) -> Vec<Slide> {
        let separator = "---";
        let split_text: Vec<&str> = text.split(separator).collect();
        let mut slides: Vec<Slide> = vec![];

        for (index, item) in split_text.iter().enumerate() {
            slides.push(Slide::new(item, index));
        }

        slides
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Slide {
    pub raw: String,
    pub content: String,
    pub number: usize,
    pub template: String,
}

impl Slide {
    fn new(text: &str, number: usize) -> Slide {
        Slide {
            raw: text.to_string(),
            content: Slide::markdown_to_html(text),
            number: number,
            template: "slide.html".to_string(),
        }
    }

    fn markdown_to_html(raw_content: &str) -> String {
        let mut output = String::new();
        let parser = Parser::new(raw_content);

        html::push_html(&mut output, parser);

        output
    }

    fn render_template(content: &str, number: &usize, template: &str) -> String {
        let tera = Tera::new("./templates/**/*").unwrap();
        let mut ctx = Context::new();

        ctx.add("content", content);
        ctx.add("number", number);

        let temp = tera.render(template, &ctx).unwrap();

        temp
    }
}
